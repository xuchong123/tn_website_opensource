<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-20
 * Time: 17:17
 */

use think\facade\Route;

Route::group(function () {

    Route::post('upload/post/file', 'Upload@uploadFile')->middleware('admin.role'); // 上传文件到本地
    Route::delete('upload/delete/file', 'Upload@deleteUploadTempFile')->middleware('admin.role'); // 删除服务器指定的文件

    Route::group('atlas', function () {
        Route::post('upload', 'Atlas@uploadImage')->middleware(['admin.role']); // 上传图集列表的图片
        Route::delete('delete', 'Atlas@deleteImage')->middleware(['admin.role']); // 删除图集列表中的图片
        Route::get('list', 'Atlas@getList'); // 获取图集列表的数据
    });

    Route::post('tn/wx/pay/notify','WxPay@notify');
    Route::post('tn/wx/pay/notify_refund','WxPay@notifyRefund');

})->prefix('\\app\\common\\controller\\')
    ->allowCrossDomain();

Route::get('atlas_category/get_all_name', 'AtlasCategory@getAllName')
    ->prefix('\\app\\admin\\controller\\')
    ->allowCrossDomain();