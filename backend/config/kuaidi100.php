<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-11-19
 * Time: 08:09
 */

return [
    // 接收订阅快递消息推送时的salt值
    'push_server_salt' => '',
    // 实时查询快递url
    'real_time_query_url'   => 'https://poll.kuaidi100.com/poll/query.do',
    // 发起快递信息推送服务url
    'create_express_push_server_url' => 'https://poll.kuaidi100.com/poll'
];