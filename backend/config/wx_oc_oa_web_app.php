<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-10-23
 * Time: 16:51
 */

return [
    'code_to_access_token' => 'https://api.weixin.qq.com/sns/oauth2/access_token?'.
        'appid=%s&secret=%s&code=%s&grant_type=authorization_code',  // 通过code获取access_token
    'refresh_access_token' => 'https://api.weixin.qq.com/sns/oauth2/refresh_token?'.
        'appid=%s&grant_type=refresh_token&refresh_token=%s',       // 通过refresh_token刷新access_token
    'check_access_token' => 'https://api.weixin.qq.com/sns/auth?'.
        'access_token=%s&openid=%s&lang=zh_CN',                     // 检验授权凭证（access_token）是否有效
    'get_user_info' => 'https://api.weixin.qq.com/sns/userinfo?'.
        'access_token=%s&openid=%s'                                 // 获取用户个人信息（UnionID机制）
];