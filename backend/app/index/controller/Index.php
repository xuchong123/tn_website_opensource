<?php
declare (strict_types = 1);

namespace app\index\controller;

class Index
{
    public function index()
    {
        return '<p>欢迎访问图鸟小程序官网，网页版即将上线</p><p>可以到微信上搜索"图鸟"小程序</p>';
    }
}
