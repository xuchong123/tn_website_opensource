<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-09
 * Time: 14:18
 */
declare(strict_types = 1);

use think\facade\Route;

Route::group(function () {
    Route::get('/','Index@index');
})->prefix('\\app\\index\\controller\\');