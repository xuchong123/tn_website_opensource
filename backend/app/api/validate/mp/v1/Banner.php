<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-02
 * Time: 21:30
 */

namespace app\api\validate\mp\v1;


use app\common\validate\BaseValidate;

class Banner extends BaseValidate
{
    protected $rule = [
        'pos_id' => 'require|number|gt:0',
        'limit' => 'require|number|gt:0'
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'limit.require' => '数量不能为空',
        'limit.number' => '数量必须为整数',
        'limit.gt' => '数量必须大于0',
    ];

    protected $scene = [
        'by_pos' => ['id','limit']
    ];
}