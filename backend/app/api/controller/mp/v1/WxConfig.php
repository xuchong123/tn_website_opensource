<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-03
 * Time: 09:32
 */

namespace app\api\controller\mp\v1;


use app\api\BaseController;

class WxConfig extends BaseController
{
    protected $deny_enname = ['mp_app_id','mp_app_secret','oc_app_id','oc_app_secret'];             //禁止获取值的字段

    /**
     * 获取对应配置的值，多个值之间用英文,隔开
     * @http GET
     * @url /config/wx?field=:field
     * :field 需要获取配置的参数
     * @return array|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getConfigValue()
    {

        $field = $this->request->param('field','');

        $fieldArray = [];
        $fieldValue = [];

        // 如果为空直接返回空数组
        if (empty($field)) {
            return $fieldValue;
        }

        // 需要获取多个field的值使用,进行分割
        if (strpos($field,',') !== false) {
            $fieldArray = explode(',',$field);
        } else {
            $fieldArray[] = $field;
        }

        // 取出每一项配置的值
        foreach ($fieldArray as $name) {
            // 判断对应的值是否为禁止获取的值
            if (in_array($name,$this->deny_enname)) {
                $fieldValue[$name] = '';
                continue;
            }

            $value = get_wx_config($name);
            // 判断是否为图片
            if (!empty($value) && preg_match('/(.*)\.\b(jpg|png|gif|bmp|jpeg|ico)\b/',$value) != 0) {
                $value = add_image_prefix($value);
            }
            $fieldValue[$name] = $value;
        }

        return tn_yes('获取微信配置成功', ['data' => $fieldValue]);
    }
}