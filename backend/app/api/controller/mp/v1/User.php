<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-02
 * Time: 19:17
 */

namespace app\api\controller\mp\v1;


use app\api\BaseController;
use app\common\model\WeChatUser as WeChatUserModel;
use app\common\model\MpApiUserToken;
use app\api\model\mp\v1\Token as TokenModel;

class User extends BaseController
{

    /**
     * 检查用户是否存在
     * @http GET
     * @url /user/exist
     * :code wxLogin获取的code
     * @return \think\response\Json
     * @throws \app\common\exception\ParameterException
     */
    public function checkUserExist() {
        $code = $this->request->get('code');

        $result = TokenModel::checkUserExist($code);

        return tn_yes("检查用户是否存在成功", [
            'data' => $result
        ]);
    }

    /**
     * 更新用户的相关信息
     * @http PUT
     * @url /user/update
     * :nick_name 用户名
     * :avatar_url 用户头像信息
     * :gender 用户性别
     * @return \think\response\Json
     */
    public function updateUserInfo()
    {
        $data = $this->request->put(['nick_name','avatar_url','gender', 'encrypData']);

        // 获取用户的uid
        $data['id'] = MpApiUserToken::getCurrentUID();
        // 设置用户来源(小程序)
        $data['from'] = 1;

        $result = WeChatUserModel::updateUserInfo($data);

        if ($result) {
            return tn_yes('更新用户信息成功');
        } else {
            return tn_no('更新用户信息失败');
        }
    }
}