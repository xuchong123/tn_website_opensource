<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-10-08
 * Time: 15:32
 */

namespace app\api\model\mp\v1;


use app\common\service\WxOcTemplateMessage;

class TNShopOrderNotifyTemplateMessage extends WxOcTemplateMessage
{
    /**
     * 发送模版消息给用户
     * @param $type
     * @param $open_id
     * @param $data
     * @param $url
     * @param string $page
     */
    public function sendOrderNotifyTemplateMessage($type, $open_id, $data)
    {
        switch ($type) {
            case 'pay_success' :
                $this->orderPaySuccessTemplate($data);
                break;
            case 'user_application_refund':
                $this->orderApplicationRefundTemplate($data);
                break;
        }

        $this->sendTemplateMessage($open_id);
    }

    /**
     * 填充订单支付成功的模版消息数据
     * @param $data
     */
    private function orderPaySuccessTemplate($data)
    {
        $this->template_id = 'OsA2QtJQNB31gaCUH27zs2L-qk9UPgZPq168_CkMH9s';

        $first_value = '有新用户购买图鸟商店商品，请注意发货';
        if ($data['virtual_product']) {
            $first_value = '(虚拟商品)' . $first_value;
        }

        $this->data = [
            'first' => [
                'value' => $first_value
            ],
            'keyword1' => [
                'value' => $data['order_no']
            ],
            'keyword2' => [
                'value' => $data['title']
            ],
            'keyword3' => [
                'value' => $data['amount']
            ],
            'keyword4' => [
                'value' => '已支付'
            ],
            'keyword5' => [
                'value' => $data['create_time']
            ],
            'remark' => [
                'value' => '请登录后台进行发货操作'
            ]
        ];
    }

    private function orderApplicationRefundTemplate($data)
    {
        $this->template_id = 'PUNnv5qTMn3WBwEwK1e35eVkONr-pSE5CRG4J738vDs';

        $this->data = [
            'first' => [
                'value' => '有用户发起了退款申请，请在72小时内尽快处理，逾期将视为同意退款，并从您的账户中自动退还到用户账号'
            ],
            'keyword1' => [
                'value' => $data['title']
            ],
            'keyword2' => [
                'value' => $data['user_name']
            ],
            'keyword3' => [
                'value' => $data['refund_amount']
            ],
            'keyword4' => [
                'value' => $data['user_application_refund_time']
            ],
            'keyword5' => [
                'value' => $data['user_application_refund_reason']
            ],
            'remark' => [
                'value' => '申请退款订单号:' . $data['order_no']
            ]
        ];
    }
}