<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-04
 * Time: 14:09
 */

namespace app\api\middleware\mp\v1;

use app\common\exception\BusinessException;
use app\common\model\Business as BusinessModel;

class CheckOperationBusinessUser
{
    public function handle($request, \Closure $next)
    {

        if (!BusinessModel::checkAdvisoryUser($request->param('id'))) {
            throw new BusinessException([
                'msg' => '已经存在咨询用户中',
                'errorCode' => 40012,
            ]);
        }

        return $next($request);
    }
}