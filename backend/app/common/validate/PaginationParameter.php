<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-15
 * Time: 17:37
 */

namespace app\common\validate;


class PaginationParameter extends BaseValidate
{
    protected $rule = [
        'page' => 'require|number|gt:0',
        'limit' => 'require|number|gt:0',
    ];

    protected $message = [
        'page.require' => '当前页参数不能为空',
        'page.number' => '当前页参数应为正整数',
        'page.gt' => '当前页参数的值要大于0',
        'limit.require' => '每页数量不能为空',
        'limit.number' => '每页数量应为正整数',
        'limit.gt' => '每页数量的值要大于0',
    ];
}