<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-10-27
 * Time: 17:45
 */

namespace app\common\model;


class ShopProductSpecsProperty extends BaseModel
{
    protected $autoWriteTimestamp = false;

    public function property()
    {
        return $this->belongsTo('ShopProductProperty', 'property_id','id');
    }
}