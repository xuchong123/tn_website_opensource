<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-11-01
 * Time: 09:53
 */

namespace app\common\model;


class OrderProduct extends BaseModel
{
    protected $autoWriteTimestamp = false;

    public function getProductImageAttr($value)
    {
        return [
            'value' => $value,
            'prefix' => $this->prefixImgUrl($value)
        ];
    }

    public function getSpecsDataAttr($value)
    {
        if (!empty($value)) {
            return json_decode($value, true);
        }
        return [];
    }
}