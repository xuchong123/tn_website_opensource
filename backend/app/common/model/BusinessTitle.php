<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-26
 * Time: 21:29
 */

namespace app\common\model;


class BusinessTitle extends BaseModel
{
    protected $autoWriteTimestamp = false;

    public function businessData()
    {
        return $this->hasMany('BusinessData','title_id','id');
    }
}