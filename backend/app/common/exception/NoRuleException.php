<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-07
 * Time: 12:56
 */

namespace app\common\exception;


class NoRuleException extends BaseException
{
    public $code = 403;
    public $msg = '没有操作权限';
    public $errorCode = 10004;
}