<?php


namespace app\common\exception;


class ProductException extends BaseException
{
    public $code = 404;
    public $msg = '商品不存在';
    public $errorCode = 40300;
}