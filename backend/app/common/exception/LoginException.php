<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-10
 * Time: 14:42
 */

namespace app\common\exception;


class LoginException extends BaseException
{
    public $code  = 403;
    public $msg = '账号或者密码错误';
    public $errorCode = 60100;
}