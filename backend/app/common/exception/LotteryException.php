<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-12-03
 * Time: 13:32
 */

namespace app\common\exception;


class LotteryException extends BaseException
{
    public $code  = 404;
    public $msg = '抽奖活动已经结束或者不存在';
    public $errorCode = 40401;
}