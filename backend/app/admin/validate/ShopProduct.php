<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-10-29
 * Time: 12:51
 */

namespace app\admin\validate;


use app\common\validate\BaseValidate;

class ShopProduct extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'title' => 'require|max:100',
        'main_image' => 'require',
        'keyword' => 'require|max:255',
        'desc' => 'require|max:255',
        'virtual_product' => 'require|number|between:0,1',
        'sales_volume' => 'number',
        'virtual_sales_volume' => 'number',
        'category_id' => 'require|number|gt:0',
        'sort' => 'require|number|gt:0',
        'recomm' => 'number|between:0,1',
        'status' => 'number|between:0,1',
        'property' => 'checkProperty',
        'specs' => 'checkSpecs',
        'parameters' => 'checkParameters'
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'title.require' => '商品标题不能为空',
        'title.max' => '商品标题最大长度为100',
        'main_image.require' => '主图不能为空',
        'keyword.require' => '商品关键词不能为空',
        'keyword.max' => '商品关键词最大长度为255',
        'desc.require' => '商品描述不能为空',
        'desc.max' => '商品描述最大长度为255',
        'virtual_product.require' => '是否为虚拟商品不能为空',
        'virtual_product.number' => '是否为虚拟商品值格式不正确',
        'virtual_product.between' => '是否为虚拟商品值格式不正确',
        'sales_volume.number' => '商品销量格式不正确',
        'virtual_sales_volume.number' => '商品虚拟销量格式不正确',
        'category_id.require' => '所属分类不能为空',
        'category_id.number' => '所属分类值不正确',
        'category_id.gt' => '所属分类值不正确',
        'sort.require' => '排序不能为空',
        'sort.number' => '排序值不正确',
        'sort.gt' => '排序值不正确',
        'recomm.number' => '是否推荐值格式不正确',
        'recomm.between' => '是否推荐值格式不正确',
        'status.number' => '推荐值格式不正确',
        'status.between' => '推荐值格式不正确'
    ];

    protected $scene = [
        'add' => ['title','main_image','keyword','desc','virtual_product','virtual_sales_volume','sort','recomm','status','property','specs'],
        'edit' => ['id','title','main_image','keyword','desc','virtual_product','virtual_sales_volume','sort','recomm','status','property','specs'],
        'get_all_specs_data' => ['title'],
    ];

    /**
     * 检查商品属性格式是否正确
     * @param $value
     * @param $rule
     * @return bool|string
     */
    public function checkProperty($value, $rule)
    {
        if (!empty($value)) {
            if (!is_array($value)) {
                return '商品属性格式错误';
            }
            foreach ($value as $item) {
                if (!isset($item['id']) || !isset($item['prop']) || !isset($item['name']) || empty($item['name'])) {
                    return '商品属性参数错误';
                }
                if (mb_strlen($item['name']) > 10) {
                    return '商品属性名称最大长度为10';
                }
            }
        }

        return true;
    }

    /**
     * 检查商品规格格式是否正确
     * @param $value
     * @param $rule
     * @param $data
     * @return bool|string
     */
    public function checkSpecs($value, $rule, $data)
    {
        if (!empty($value)) {
            if (!is_array($value)) {
                return '商品规格格式错误';
            }

            if (empty($data['property'])) {
                if (count($value) > 1) {
                    return '没有商品属性时，商品规格不能操作一种';
                }
            }

            foreach ($value as $item) {
                if (!isset($item['id']) ||
                    !isset($item['product_image']) ||
                    !isset($item['original_price']) ||
                    !isset($item['selling_price']) ||
                    !isset($item['stock']) ||
                    mb_strlen($item['product_image']) > 255 ||
                    !is_numeric((string) $item['original_price']) ||
                    !is_numeric((string) $item['selling_price']) ||
                    !ctype_digit((string) $item['stock'])
                ) {
                    return '商品规格参数错误';
                }

                // 如果有属性，判断属性对应的数值是否正确
                if (!empty($data['property'])) {
                    $property_prop = array_column($data['property'], 'prop');
                    foreach ($property_prop as $prop_item) {
                        if (!isset($item[$prop_item]) || empty($item[$prop_item]) || mb_strlen($item[$prop_item]) > 30) {
                            return '商品规格属性参数错误';
                        }
                    }
                }
            }
        } else {
            return '商品规格不能为空';
        }

        return true;
    }

    /**
     * 检查商品产品参数是否正确
     * @param $value
     * @param $rule
     * @return bool|string
     */
    public function checkParameters($value, $rule)
    {
        if (!empty($value)) {
            if (!is_array($value)) {
                return '商品商品参数格式错误';
            }

            foreach ($value as $item) {
                if (!isset($item['name']) ||
                    !isset($item['value']) ||
                    mb_strlen($item['name']) > 30 ||
                    mb_strlen($item['value']) > 100
                ) {
                    return '商品商品参数值格式错误';
                }
            }
        }

        return true;
    }
}