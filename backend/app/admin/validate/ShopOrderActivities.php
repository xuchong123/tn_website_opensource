<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-25
 * Time: 13:59
 */

namespace app\admin\validate;


use app\common\validate\BaseValidate;

class ShopOrderActivities extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'activities_id' => 'require|number|gt:0',
        'product_specs_id' => 'require|number|gt:0',
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'activities_id.require' => '所属活动不能为空',
        'activities_id.number' => '所属活动不能为空编号格式错误',
        'activities_id.gt' => '所属活动不能为空编号格式错误',
        'product_specs_id.require' => '商品属性id不能为空',
        'product_specs_id.number' => '商品属性id必须为整数',
        'product_specs_id.gt' => '商品属性id必须大于0',
    ];

    protected $scene = [
        'get_award' => ['activities_id', 'product_specs_id'],
        'check_receive' => ['activities_id', 'product_specs_id'],
        'check_qualification' => ['activities_id', 'product_specs_id']
    ];
}