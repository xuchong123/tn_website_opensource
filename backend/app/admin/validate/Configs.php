<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-08
 * Time: 10:07
 */

namespace app\admin\validate;


use app\common\validate\BaseValidate;

class Configs extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'pid' => 'require|number',
        'cn_name' => 'require|max:80',
        'en_name' => 'checkEnNameRequire|max:80|alphaDash',
        'tips' => 'max:255',
        'type' => 'require|between:0,11',
        'sort' => 'require|number|gt:0',
        'status' => 'require|number|between:0,1'
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'pid.require' => 'pid不能为空',
        'pid.number' => 'pid必须为整数',
        'cn_name.require' => '中文名不能为空',
        'cn_name.max' => '中文名最大长度为80',
        'en_name.checkEnNameRequire' => '字段名称不能为空',
        'en_name.max' => '字段名称最大长度为80',
        'en_name.alphaDash' => '字段名称格式不正确',
        'tips.max' => '提示最大长度不能超过255',
        'type.require' => '类型不能为空',
        'type.between' => '类型的格式不正确',
        'sort.require' => '排序不能为空',
        'sort.number' => '排序格式不正确',
        'sort.gt' => '排序格式不正确',
        'status.require' => '状态不能为空',
        'status.number' => '状态格式不正确',
        'status.between' => '状态格式不正确',
    ];

    protected $scene = [
        'add' => ['pid','cn_name','en_name','tips','type','sort','status'],
        'edit' => ['id','pid','cn_name','en_name','tips','type','sort','status']
    ];

    protected function checkEnNameRequire($value,$rule = ''
        ,$data = [],$field = '')
    {
        if ($data['pid'] !== 0) {
            return !empty($value) || '0' == $value;
        }
        return true;
    }
}