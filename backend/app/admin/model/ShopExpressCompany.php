<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-11-14
 * Time: 13:08
 */

namespace app\admin\model;


use app\common\exception\HaveUseException;
use app\common\exception\ParameterException;
use app\common\model\BaseModel;
use app\common\model\OrderExpress;
use app\common\service\ExcelFileHandle;
use app\common\validate\IDCollection;
use think\facade\App;
use think\facade\Filesystem;
use think\model\concern\SoftDelete;
use app\admin\validate\ShopExpressCompany as Validate;

class ShopExpressCompany extends BaseModel
{
    protected $hidden = ['update_time','delete_time'];

    use SoftDelete;
    protected $deleteTime = 'delete_time';

    /**
     * 获取商店快递公司的分页数据
     * @param array $params
     * @return \think\Paginator
     */
    public static function getPaginationList(array $params)
    {
        static::validatePaginationData($params);

        $static = new static();

        foreach ($params as $name => $value) {
            $value = trim($value);
            switch ($name) {
                case 'name':
                    if (!empty($value)) {
                        $like_text = '%' . $value . '%';
                        $static = $static->whereLike('name', $like_text);
                    }
                    break;
            }
        }

        return $static
            ->paginate([
                'page' => $params['page'],
                'list_rows' => $params['limit']
            ], false);
    }

    /**
     * 获取全部的商店快递公司信息
     * @return array
     */
    public static function getAllCompanyName($name)
    {
        $company = static::where([['name','like','%'.$name.'%']])
            ->field('id,name,code')
            ->select();

        if ($company->isEmpty()) {
            return [];
        }

        $company = $company->toArray();

        return $company;
    }

    /**
     * 添加商店快递公司信息
     * @param array $data
     * @return bool
     */
    public static function addShopExpressCompany(array $data)
    {
        $validate = new Validate();
        if (!$validate->scene('add')->check($data)) {
            throw new ParameterException([
                'msg' => $validate->getError()
            ]);
        }

        $static = static::create($data);

        if (isset($static->id) && $static->id > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 通过Excel文件导入商店快递公司信息
     * @param $file
     * @return bool
     */
    public static function addShopExpressCompanyByExcel($file)
    {
        $validate = new Validate();
        if (!$validate->scene('add_excel')->check([
            'excel_file' => $file
        ])) {
            throw new ParameterException([
                'msg' => $validate->getError()
            ]);
        }

        $save_name = '';
        try {
            // 先将文件保存起来，再进行操作
            $save_name = Filesystem::disk('public')->putFile('uploads/shop_express_company_excel', $file, 'md5');
            $save_name = App::getRootPath() . 'public/storage/' . $save_name;

            $excel_data = ExcelFileHandle::readExcelDataByColumnAndRow($save_name, 'B', 3);

            // 读取数据库中的信息，比较当前插入的内容和已存在的内容是否存在冲突
            $exist_company_data = static::field('id,name,code')
                ->select();
            // 筛选出新的快递公司
            if (!$exist_company_data->isEmpty()) {
                $exist_company_data = $exist_company_data->toArray();
                $exist_code_data = array_column($exist_company_data, 'code');
                $new_company_data = array_filter($excel_data, function ($item) use ($exist_code_data) {
                    return !in_array($item[1], $exist_code_data);
                });

                // 如果有相同的快递公司则进行更新（以code编码为标准）
//                if (count($new_company_data) != count($excel_data)) {
//                    $exist_data = [];
//                    foreach ($exist_company_data as $company_item) {
//                        $exist_data[$company_item['code']] = [
//                            'id' => $company_item['id'],
//                            'name' => $company_item['name']
//                        ];
//                    }
//                    $update_company_data = array_filter($excel_data, function ($item) use ($exist_code_data) {
//                        return in_array($item[1], $exist_code_data);
//                    });
//                    $update_company_data = array_map(function ($item) use ($exist_data) {
//                        return [
//                            'id' => $exist_data[$item[1]]['id'],
//                            'name' => $exist_data[$item[1]]['name']
//                        ];
//                    }, $update_company_data);
//                }
            } else {
                $new_company_data = $excel_data;
            }

            $new_company_data = array_map(function ($item) {
                return [
                    'name' => $item[0],
                    'code' => $item[1]
                ];
            }, $new_company_data);
            $new_company_data = array_values($new_company_data);

            if (!empty($new_company_data)) {

                (new static())->saveAll($new_company_data);
            }


        } catch (\Exception $e) {
            if (file_exists($save_name)){
                unlink($save_name);
            }
            return false;
        }

        if (file_exists($save_name)){
            unlink($save_name);
        }
        return true;
    }

    /**
     * 编辑商店快递公司信息
     * @param array $data
     * @return bool
     */
    public static function editShopExpressCompany(array $data)
    {
        $validate = new Validate();
        if (!$validate->scene('edit')->check($data)) {
            throw new ParameterException([
                'msg' => $validate->getError()
            ]);
        }

        $static = static::find($data['id']);
        $result = $static->allowField(['id','name','code'])
            ->save($data);

        if ($result !== false) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 删除指定id的商店快递公司信息
     * @param $ids
     * @return bool
     */
    public static function deleteExpressCompany($ids)
    {
        $validate = new IDCollection();
        if (!$validate->check(['ids' => $ids])) {
            throw new ParameterException([
                'msg' => $validate->getError()
            ]);
        }

        if (!is_array($ids)) {
            $ids = explode(',', $ids);
        }

        $atlas_data = OrderExpress::where('express_company_id','in', $ids)
            ->count('id');

        if (!empty($atlas_data)) {
            throw new HaveUseException([
                'msg' => '已有图鸟商店使用了当前快递公司信息'
            ]);
        }

        if (static::destroy($ids) !== false) {
            return true;
        } else {
            return false;
        }
    }
}