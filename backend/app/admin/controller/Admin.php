<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-09
 * Time: 13:53
 */
declare(strict_types = 1);

namespace app\admin\controller;


use app\admin\BaseController;
use app\api\model\oc\v1\WeChatUserAdminAuth;
use app\common\exception\SuperAdminException;
use app\common\exception\UrlNoPresenceException;
use app\admin\model\Admin as AdminModel;
use app\common\exception\UserException;

class Admin extends BaseController
{
    /**
     * 管理员登陆
     * @http POST
     * @url /admin/login
     * @return \think\response\Json
     */
    public function login()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['user_name','password']);

        $token = (string)AdminModel::login($data);

        return tn_yes('登陆成功', [
            'token' => $token
        ]);
    }

    /**
     * 管理员退出
     * @http POST
     * @url /admin/logout
     * @return \think\response\Json
     */
    public function logout()
    {
        $this->checkPostUrl();

        // 获取token的数据
        $token_data = $this->request->token_data;

        AdminModel::logout($token_data['uid']);

        return tn_yes('退出成功');
    }

    /**
     * 获取对应管理员的路由信息
     * @http GET
     * @url /admin/routes
     * @return \think\response\Json
     */
    public function routes()
    {
        // 获取token的数据
        $token_data = $this->request->token_data;

        $routes = AdminModel::getRoutes($token_data);

        return tn_yes('获取数据成功', $routes);
    }

    /**
     * 获取对应管理员的信息
     * @http GET
     * @url /admin/info
     * @return \think\response\Json
     */
    public function info()
    {
        // 获取token的数据
        $token_data = $this->request->token_data;

        $info = AdminModel::getInfo($token_data);

        return tn_yes('获取用户信息成功', $info);
    }

    /**
     * 获取管理员列表数据
     * @http GET
     * @url /admin/list
     * @return \think\response\Json
     */
    public function getList()
    {
        $params = $this->request->get();

        // 获取token的数据
        $token_data = $this->request->token_data;

        $data = AdminModel::getPaginationList($token_data,$params);

        return tn_yes('获取管理员列表数据成功',return_vue_element_admin_pagination_data($data));
    }

    /**
     * 根据id获取管理员信息
     * @http GET
     * @url /admin/get_id
     * @return \think\response\Json
     */
    public function getByID()
    {
        $id = $this->request->get('id');

        // 获取token的数据
        $token_data = $this->request->token_data;

        if ($token_data['uid'] != 1 && $id == 1) {
            throw new SuperAdminException();
        }

        $data = AdminModel::getAdminByID($id);

        return tn_yes('获取管理员信息成功',['data' => $data]);
    }

    /**
     * 添加管理员
     * @http POST
     * @url /admin/add
     * @return \think\response\Json
     */
    public function addAdmin()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['user_name','nick_name','password','checkpass','role_id','gender'=>0,'avatar','introduction','status'=>0]);

        $result = AdminModel::addAdmin($data);

        if ($result) {
            $this->request->log_content = '添加管理员成功';
            return tn_yes('添加管理员成功');
        } else {
            $this->request->log_content = '添加管理员失败';
            return tn_no('添加管理员失败');
        }
    }

    /**
     * 编辑管理员
     * @http PUT
     * @url /admin/edit
     * @return \think\response\Json
     */
    public function editAdmin()
    {
        $this->checkPutUrl();

        $data = $this->request->put(['id','user_name','nick_name','password','checkpass','role_id','gender'=>0,'avatar','introduction','status'=>0]);

        $data['token_data'] = $this->request->token_data;

        $result = AdminModel::editAdmin($data);

        if ($result) {
            $this->request->log_content = '编辑管理员成功';
            return tn_yes('编辑管理员成功');
        }else {
            $this->request->log_content = '编辑管理员失败';
            return tn_no('编辑管理员失败');
        }
    }

    /**
     * 更新管理员信息
     * @http PUT
     * @url /admin/update
     * @return \think\response\Json
     */
    public function updateAdmin()
    {
        $this->checkPutUrl();

        // 获取put的数据
        $data = $this->request->put();

        $this->checkUpdateValidate($data);

        $result = AdminModel::updateInfo($data['id'],[
            $data['field'] => $data['value']
        ]);

        if ($result) {
            $this->request->log_content = '更新管理员信息成功';
            return tn_yes('更新管理员信息成功');
        }else {
            $this->request->log_content = '更新管理员信息失败';
            return tn_no('更新管理员信息失败');
        }
    }

    /**
     * 删除指定的管理员信息
     * @http DELETE
     * @url /admin/delete
     * @return \think\response\Json
     */
    public function deleteAdmin()
    {
        $this->checkDeleteUrl();

        $data = $this->request->delete('ids');

        $result = AdminModel::delByIDs($data);

        if ($result) {
            $this->request->log_content = '删除管理员信息成功';
            return tn_yes('删除管理员信息成功');
        }else {
            $this->request->log_content = '删除管理员信息失败';
            return tn_no('删除管理员信息失败');
        }
    }

    /**
     * 微信扫码登录管理员
     * @http get
     * @url /admin/we_chat_login
     * @return \think\response\Json
     */
    public function weChatAdminLogin()
    {
        $data = $this->request->get(['code']);

//        $code = (string)AdminModel::weChatAdminLogin($data);
//        var_dump($code);
//        return ;
        try {
            $code = (string)AdminModel::weChatAdminLogin($data);
        } catch (UserException $e) {
            return '<p style="position: absolute;left: 50%;top: 45%;transform: translate(-50%, -50%);">登录出错，用户不存在</p>';
        } catch (\Exception $e) {
            return '<p style="position: absolute;left: 50%;top: 45%;transform: translate(-50%, -50%);">登录出错，出错信息为: '.$e->getMessage().'</p>';
        }

        $url = 'https://website.tnkjapp.com/fontend/#/wx-auth-redirect?code=' . $code;
        return redirect($url);

//        return `<script>window.localStorage.setItem('tn-wx-admin-oauth-code', $code)</script>`;
    }

    /**
     * 通过微信管理员登录的code信息获取token数据
     * @http post
     * @url /admin/get_we_chat_code_token
     * @return \think\response\Json
     */
    public function getWeChatLoginToken()
    {
        $this->checkPostUrl();

        $code = $this->request->post('code', '');

        $token = AdminModel::getJWTTokenByCode($code);

        return tn_yes('获取token成功', [
            'token' => $token
        ]);
    }

    /**
     * 获取微信管理员列表数据
     * @http get
     * @url /admin/list_we_chat_admin
     * @return \think\response\Json
     */
    public function weChatAdminUserList()
    {
        $params = $this->request->get();

        $data = AdminModel::getWeChatAdminPaginateList($params);

        return tn_yes('获取管理员列表数据成功',return_vue_element_admin_pagination_data($data));
    }

    /**
     * 添加指定微信用户管理员
     * @http GET
     * @url /admin/add_wx_auth/:id/
     * :id => 绑定的管理员id
     */
    public function addWXUserAdmin()
    {
        $id = $this->request->param('id', 0);

        try {
            $status = AdminModel::addWeChatUserAdmin($id);
        } catch (\Exception $e) {
            return '添加微信用户失败，失败信息为：' . $e->getMessage();
        }

//        $status = AdminModel::addWeChatUserAdmin($id);
//        var_dump($status);
//        return '读取信息成功' . json_encode($status, JSON_UNESCAPED_UNICODE);

        switch ($status) {
            case -1:
                return '当前绑定的用户状态为禁用，请选择其他管理员';
            case -2:
//                $img_url = WeChatUserAdminAuth::generateAdminAuthQRCode();
//                return '<p>您还没授权本平台，请扫码授权</p><p><img alt="微信用户授权" src="'.$img_url.'"></p>';
                return '请在图鸟微信小程序上授权登录一下，保证数据唯一性';
        }

        return '<p style="position: absolute;left: 50%;top: 45%;transform: translate(-50%, -50%);">绑定管理员成功</p>';

    }

    /**
     * 删除微信管理员
     * @http delete
     * @url /admin/delete_we_chat_admin
     * @return \think\response\Json
     */
    public function deleteWeChatAdmin()
    {
        $this->checkDeleteUrl();

        $data = $this->request->delete();

        $result = AdminModel::deleteWeChatAdmin($data);

        if ($result) {
            $this->request->log_content = '删除微信管理员信息成功';
            return tn_yes('删除微信管理员信息成功');
        }else {
            $this->request->log_content = '删除微信管理员信息失败';
            return tn_no('删除微信管理员信息失败');
        }
    }
}