<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-30
 * Time: 11:37
 */

namespace app\admin\controller;


use app\admin\BaseController;
use app\common\model\Content as ContentModel;

class Content extends BaseController
{
    /**
     * 获取内容的分页数据
     * @http get
     * @url /content/list
     * @return \think\response\Json
     */
    public function getList()
    {
        $params = $this->request->get();

        $data = ContentModel::getPaginationList($params);

        return tn_yes('获取内容的分页数据', return_vue_element_admin_pagination_data($data));
    }

    /**
     * 获取内容的指定用户信息
     * @http get
     * @url /content/get_operation_user
     * @return \think\response\Json
     */
    public function getOperationUser()
    {
        $params = $this->request->get();

        $data = ContentModel::getOperationUserPaginationList($params);

        return tn_yes('获取内容的指定用户信息成功',return_vue_element_admin_pagination_data($data));
    }

    /**
     * 根据id获取内容信息
     * @http get
     * @url /content/get_id
     * @return \think\response\Json
     */
    public function getByID()
    {
        $id = $this->request->get('id');

        $data = ContentModel::getDataWithID($id);

        return tn_yes('获取内容信息信息成功', ['data' => $data]);
    }

    /**
     * 获取指定栏目下内容的数量
     * @http get
     * @url /content/get_category_children
     * @return \think\response\Json
     */
    public function getCategoryChildrenCount()
    {
        $category_id = $this->request->get('category_id', 0);

        $count = ContentModel::getChildrenCount($category_id, 'category_id');

        return tn_yes('获取指定栏目下内容的数量', ['count' => $count]);
    }

    /**
     * 添加内容信息
     * @http post
     * @url /content/add
     * @return \think\response\Json
     */
    public function addContent()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['title','main_image','category_id','model_id','table_id','product_id','view_count','like_count','share_count','sort','recomm','status','fields_data']);

        $result = ContentModel::addContent($data);

        if ($result) {
            $this->request->log_content = '添加内容信息成功';
            return tn_yes('添加内容信息成功');
        } else {
            $this->request->log_content = '添加内容信息失败';
            return tn_no('添加内容信息失败');
        }
    }

    /**
     * 编辑内容信息
     * @http put
     * @url /content/edit
     * @return \think\response\Json
     */
    public function editContent()
    {
        $this->checkPutUrl();

        $data = $this->request->put(['id','title','main_image','category_id','model_id','table_id','product_id','view_count','like_count','share_count','sort','recomm','status','fields_data']);

        $result = ContentModel::editContent($data);

        if ($result) {
            $this->request->log_content = '编辑内容信息成功';
            return tn_yes('编辑内容信息成功');
        } else {
            $this->request->log_content = '编辑内容信息失败';
            return tn_no('编辑内容信息失败');
        }
    }

    /**
     * 更新内容信息
     * @http PUT
     * @url /content/update
     * @return \think\response\Json
     */
    public function updateContent()
    {
        $this->checkPutUrl();

        // 获取put的数据
        $data = $this->request->put();

        $this->checkUpdateValidate($data);

        $result = ContentModel::updateInfo($data['id'],[
            $data['field'] => $data['value']
        ]);

        if ($result) {
            $this->request->log_content = '更新内容信息成功';
            return tn_yes('更新内容信息成功');
        }else {
            $this->request->log_content = '更新内容信息失败';
            return tn_no('更新内容信息失败');
        }
    }

    /**
     * 删除指定的内容信息
     * @http DELETE
     * @url /content/delete
     * @return \think\response\Json
     */
    public function deleteContent()
    {
        $this->checkDeleteUrl();

        $data = $this->request->delete('ids');

        $result = ContentModel::delByIDs($data);

        if ($result) {
            $this->request->log_content = '删除内容信息成功';
            return tn_yes('删除内容信息成功');
        }else {
            $this->request->log_content = '删除内容信息失败';
            return tn_no('删除内容信息失败');
        }
    }
}