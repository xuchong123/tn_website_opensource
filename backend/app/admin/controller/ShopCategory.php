<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-10-31
 * Time: 09:59
 */

namespace app\admin\controller;


use app\admin\BaseController;
use app\common\model\ShopCategory as ShopCategoryModel;

class ShopCategory extends BaseController
{
    /**
     * 获取商店分类分页信息
     * @http get
     * @url /shop_category/list
     * @return \think\response\Json
     */
    public function getList()
    {
        $params = $this->request->get();

        $data = ShopCategoryModel::getPaginationList($params);

        return tn_yes('获取商店分类分页信息成功', return_vue_element_admin_pagination_data($data));
    }

    /**
     * 获取商店分类的全部标题
     * @http get
     * @url /shop_product/get_all_title
     * @return \think\response\Json
     */
    public function getAllTitle()
    {
        $data = ShopCategoryModel::getDataWithField([['status','=',1]],['id','title'],['sort' => 'ASC']);

        $data = $data->isEmpty() ? [] : $data->toArray();

        return tn_yes('获取商店分类全部标题信息成功', ['data' => $data]);
    }

    /**
     * 获取数据的数量
     * @http get
     * @url /shop_category/get_all_count
     * @return \think\response\Json
     */
    public function getAllCount()
    {
        $count = ShopCategoryModel::getDataAllCount();

        return tn_yes('获取全部数据数量成功', ['count' => $count]);
    }

    /**
     * 根据id获取对应商店分类信息
     * @http get
     * @url /shop_category/get_id
     * @return \think\response\Json
     */
    public function getByID()
    {
        $id = $this->request->get('id');

        $data = ShopCategoryModel::getDataWithID($id);

        return tn_yes('获取商店分类信息成功', ['data' => $data]);
    }

    /**
     * 添加商店分类信息
     * @http post
     * @url /shop_category/add
     * @return \think\response\Json
     */
    public function addCategory()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['title','sort','status'=>0]);

        $result = ShopCategoryModel::addCategory($data);

        if ($result) {
            $this->request->log_content = '添加商店分类成功';
            return tn_yes('添加商店分类成功');
        }else {
            $this->request->log_content = '添加商店分类失败';
            return tn_no('添加商店分类失败');
        }
    }

    /**
     * 编辑商店分类信息
     * @http put
     * @url /shop_category/edit
     * @return \think\response\Json
     */
    public function editCategory()
    {
        $this->checkPutUrl();

        $data = $this->request->put(['id','title','sort','status'=>0]);

        $result = ShopCategoryModel::editCategory($data);

        if ($result) {
            $this->request->log_content = '编辑商店分类成功';
            return tn_yes('编辑商店分类成功');
        }else {
            $this->request->log_content = '编辑商店分类失败';
            return tn_no('编辑商店分类失败');
        }
    }

    /**
     * 更新商店分类信息
     * @http put
     * @url /shop_category/update
     * @return \think\response\Json
     */
    public function updateCategory()
    {
        $this->checkPutUrl();

        // 获取put的数据
        $data = $this->request->put();

        $this->checkUpdateValidate($data);

        $result = ShopCategoryModel::updateInfo($data['id'],[
            $data['field'] => $data['value']
        ]);

        if ($result) {
            $this->request->log_content = '更新商店分类成功';
            return tn_yes('更新商店分类成功');
        }else {
            $this->request->log_content = '更新商店分类失败';
            return tn_no('更新商店分类失败');
        }
    }

    /**
     * 删除商店分类信息
     * @http delete
     * @url /shop_category/delete
     * @return \think\response\Json
     */
    public function deleteCategory()
    {
        $this->checkDeleteUrl();

        $ids = $this->request->delete('ids');

        $result = ShopCategoryModel::delByIDs($ids);

        if ($result) {
            $this->request->log_content = '删除商店分类成功';
            return tn_yes('删除商店分类成功');
        }else {
            $this->request->log_content = '删除商店分类失败';
            return tn_no('删除商店分类失败');
        }
    }
}