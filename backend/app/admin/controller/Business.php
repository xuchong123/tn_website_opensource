<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-27
 * Time: 10:09
 */

namespace app\admin\controller;


use app\admin\BaseController;
use app\common\model\Business as BusinessModel;

class Business extends BaseController
{
    /**
     * 获取业务分页数据
     * @http get
     * @url /business/list
     * @return \think\response\Json
     */
    public function getList()
    {
        $params = $this->request->get();

        $data = BusinessModel::getPaginationList($params);

        return tn_yes('获取业务分页数据成功', return_vue_element_admin_pagination_data($data));
    }

    /**
     * 根据id获取业务信息
     * @http get
     * @url /business/get_id
     * @return \think\response\Json
     */
    public function getByID()
    {
        $id = $this->request->get('id');

        $data = BusinessModel::getBusinessByID($id);

        return tn_yes('获取业务信息成功', ['data' => $data]);
    }

    /**
     * 获取业务的指定用户信息
     * @http get
     * @url /business/get_operation_user
     * @return \think\response\Json
     */
    public function getOperationUser()
    {
        $params = $this->request->get();

        $data = BusinessModel::getOperationUserPaginationList($params);

        return tn_yes('获取业务的指定用户信息成功',return_vue_element_admin_pagination_data($data));
    }

    /**
     * 添加业务信息
     * @http post
     * @url /business/add
     * @return \think\response\Json
     */
    public function addBusiness()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['title','query_count','sort','status'=>0,'business_data']);

        $result = BusinessModel::addBusiness($data);

        if ($result) {
            $this->request->log_content = '添加业务成功';
            return tn_yes('添加业务成功');
        } else {
            $this->request->log_content = '添加业务失败';
            return tn_no('添加业务失败');
        }
    }

    /**
     * 编辑业务信息
     * @http put
     * @url /business/edit
     * @return \think\response\Json
     */
    public function editBusiness()
    {
        $this->checkPutUrl();

        $data = $this->request->put(['id','title','query_count','sort','status'=>0,'business_data']);

        $result = BusinessModel::editBusiness($data);

        if ($result) {
            $this->request->log_content = '编辑业务成功';
            return tn_yes('编辑业务成功');
        } else {
            $this->request->log_content = '编辑业务失败';
            return tn_no('编辑业务失败');
        }
    }

    /**
     * 更新业务信息
     * @http PUT
     * @url /business/update
     * @return \think\response\Json
     */
    public function updateBusiness()
    {
        $this->checkPutUrl();

        // 获取put的数据
        $data = $this->request->put();

        $this->checkUpdateValidate($data);

        $result = BusinessModel::updateInfo($data['id'],[
            $data['field'] => $data['value']
        ]);

        if ($result) {
            $this->request->log_content = '更新业务信息成功';
            return tn_yes('更新业务信息成功');
        }else {
            $this->request->log_content = '更新业务信息失败';
            return tn_no('更新业务信息失败');
        }
    }

    /**
     * 删除业务信息
     * @http delete
     * @url /business/delete
     * @return \think\response\Json
     */
    public function deleteBusiness()
    {
        $this->checkDeleteUrl();

        $ids = $this->request->delete('ids');

        $result = BusinessModel::delByIDs($ids);

        if ($result) {
            $this->request->log_content = '删除业务成功';
            return tn_yes('删除业务成功');
        } else {
            $this->request->log_content = '删除业务失败';
            return tn_no('删除业务失败');
        }
    }
}