/*
 Navicat Premium Data Transfer

 Source Server         : 本地数据库
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : localhost:3306
 Source Schema         : tuniao_vue_bof

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : 65001

 Date: 23/05/2020 15:00:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tn_admin
-- ----------------------------
DROP TABLE IF EXISTS `tn_admin`;
CREATE TABLE `tn_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员主键id',
  `user_name` varchar(20) NOT NULL DEFAULT '' COMMENT '登陆用户名',
  `password` varchar(80) NOT NULL DEFAULT '' COMMENT '登陆密码',
  `gender` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '管理员性别(0 男 1 女)',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '管理员头像',
  `nick_name` varchar(20) NOT NULL DEFAULT '' COMMENT '管理员别名',
  `introduction` varchar(255) NOT NULL DEFAULT '' COMMENT '管理员简介',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '管理员开启状态',
  `role_id` int(255) NOT NULL DEFAULT '0' COMMENT '所属角色的id',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='保存管理员信息';

-- ----------------------------
-- Records of tn_admin
-- ----------------------------
BEGIN;
INSERT INTO `tn_admin` VALUES (1, 'super_admin', '$2y$10$xQH5JHy6WRGywezjTtgh3.BmGtQkhufo/lf1Wlzs6h3kXbfGXcBju', 0, '/storage/uploads/img_list/c6/aab26376f3f6dc3c5a737784e45985.png', '超级管理员', '图鸟超级管理员', 1, 1, 1588213740, 1590207908, NULL);
INSERT INTO `tn_admin` VALUES (2, 'admin', '$2y$10$.LopVHxUhiQ0kAI/Gnj2qOvbhnnVJmYi36ev0eWm97UHrAKCb4C.q', 0, '/storage/uploads/img_list/27/5c2e56b7b0967f2a52ad19fe0bb765.jpg', '图鸟管理员', '普通管理员', 1, 2, 1588215393, 1590207939, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_atlas
-- ----------------------------
DROP TABLE IF EXISTS `tn_atlas`;
CREATE TABLE `tn_atlas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '图集主键id',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '图片的名称，对应上传文件的原名称',
  `img_path` varchar(255) NOT NULL DEFAULT '' COMMENT '图片保存的路径',
  `md5` varchar(34) NOT NULL DEFAULT '' COMMENT '记录图片的md5值，用于去重复',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '图片的保存类型（1、保存在本地）',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图片所属分类',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_md5_category` (`name`,`md5`,`category_id`) USING BTREE,
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='保存图集的图片信息';

-- ----------------------------
-- Records of tn_atlas
-- ----------------------------
BEGIN;
INSERT INTO `tn_atlas` VALUES (1, 'logo.png', '/storage/uploads/img_list/c6/aab26376f3f6dc3c5a737784e45985.png', 'c6aab26376f3f6dc3c5a737784e45985', 1, 1, 1590207902, 1590207902, NULL);
INSERT INTO `tn_atlas` VALUES (2, 'logo.jpg', '/storage/uploads/img_list/27/5c2e56b7b0967f2a52ad19fe0bb765.jpg', '275c2e56b7b0967f2a52ad19fe0bb765', 1, 2, 1590207934, 1590207934, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_atlas_category
-- ----------------------------
DROP TABLE IF EXISTS `tn_atlas_category`;
CREATE TABLE `tn_atlas_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '图集类目主键id',
  `name` varchar(60) NOT NULL DEFAULT '' COMMENT '图集类目名称',
  `sort` mediumint(8) unsigned NOT NULL DEFAULT '1' COMMENT '图集排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否开启图集',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='保存图集类目的相关信息';

-- ----------------------------
-- Records of tn_atlas_category
-- ----------------------------
BEGIN;
INSERT INTO `tn_atlas_category` VALUES (1, '图鸟官方图标', 1, 1, 1589794899, 1589797112, NULL);
INSERT INTO `tn_atlas_category` VALUES (2, '头像', 2, 1, 1589797169, 1589797169, NULL);
INSERT INTO `tn_atlas_category` VALUES (3, '杂项', 3, 1, 1590125624, 1590125624, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_auth_menu
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_menu`;
CREATE TABLE `tn_auth_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色菜单主键id',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '角色菜单上级id',
  `name` varchar(60) NOT NULL DEFAULT '' COMMENT '角色菜单路由名字',
  `title` varchar(60) NOT NULL DEFAULT '' COMMENT '角色菜单侧边栏喝面包屑展示的名称',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '角色菜单路由地址',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '角色菜单文件所在路径（为空则为Layout）',
  `redirect` varchar(255) NOT NULL DEFAULT '' COMMENT '角色菜单重定向地址',
  `icon` varchar(80) NOT NULL DEFAULT '' COMMENT '角色菜单的图标',
  `is_link` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '标记角色菜单是否为链接',
  `hidden` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '标记角色菜单是否隐藏不显示在侧边栏（1 隐藏 0 显示）',
  `is_click_in_breadcrumb` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '标记角色菜单是否可以在面包屑中点击',
  `always_show` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '标记角色菜单是否总是显示在侧边栏（1 永远显示 0 不显示）',
  `no_cache` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '标记角色菜单是否被缓存（1 缓存 0 不缓存）',
  `breadcrumb_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '标记角色菜单是否显示在面包屑（1 显示在面包屑 0 不显示在面包屑）',
  `public_menu` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '标记角色菜单是否为公共菜单',
  `sort` mediumint(8) unsigned NOT NULL DEFAULT '1' COMMENT '角色菜单排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '角色菜单状态',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COMMENT='保存角色菜单的数据';

-- ----------------------------
-- Records of tn_auth_menu
-- ----------------------------
BEGIN;
INSERT INTO `tn_auth_menu` VALUES (1, 0, 'Admin', '管理员', '/admin', '', '/admin/admin/list', 'tn_admin', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1588504538, 1588599355, NULL);
INSERT INTO `tn_auth_menu` VALUES (2, 1, 'Admin-Admin', '管理员人员管理', 'admin', 'admin/admin/index', '/admin/admin/list', 'tn_admin_1', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1588560688, 1589782361, NULL);
INSERT INTO `tn_auth_menu` VALUES (3, 2, 'Admin-Admin-List', '人员列表', 'list', 'admin/admin/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1588562226, 1588599355, NULL);
INSERT INTO `tn_auth_menu` VALUES (4, 1, 'Admin-Auth', '管理员权限管理', 'auth', 'admin/auth/index', '', 'tn_admin_auth', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1588570663, 1588667986, NULL);
INSERT INTO `tn_auth_menu` VALUES (5, 4, 'Admin-Auth-Role-List', '角色列表', 'role', 'admin/auth/role/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1588570730, 1588667986, NULL);
INSERT INTO `tn_auth_menu` VALUES (6, 4, 'Admin-Auth-Menu-List', '菜单列表', 'menu', 'admin/auth/menu/list', '', '', 0, 0, 1, 0, 0, 1, 0, 3, 1, 1588570858, 1588860419, NULL);
INSERT INTO `tn_auth_menu` VALUES (7, 0, 'Tuniao', '图鸟官网', 'https://tuniaokj.com/', '', '', 'international', 1, 0, 1, 0, 0, 1, 1, 4, 1, 1588571120, 1589782216, NULL);
INSERT INTO `tn_auth_menu` VALUES (8, 4, 'Admin-Auth-Rule-List', '规则列表', 'rule', 'admin/auth/rule/list', '', '', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1588852174, 1588860422, NULL);
INSERT INTO `tn_auth_menu` VALUES (9, 0, 'System', '系统', '/system', '', '', 'xitongshezhi', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1588916619, 1589505965, NULL);
INSERT INTO `tn_auth_menu` VALUES (10, 9, 'SystemConfig-List', '配置项列表', 'config-list', 'system-config/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1588916672, 1589505986, NULL);
INSERT INTO `tn_auth_menu` VALUES (11, 9, 'SystemConfig-Menu', '系统配置', 'menu', 'system-config/menu', '', '', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1589015089, 1589015089, NULL);
INSERT INTO `tn_auth_menu` VALUES (12, 9, 'SystemLog-List', '系统日志', 'log-list', 'system-log/list', '', '', 0, 0, 1, 0, 0, 1, 0, 3, 1, 1589505946, 1589506013, NULL);
INSERT INTO `tn_auth_menu` VALUES (13, 0, 'Atlas', '图集', '/atlas', '', '/atlas/category', 'table', 0, 0, 1, 0, 0, 1, 1, 3, 1, 1589782312, 1589782312, NULL);
INSERT INTO `tn_auth_menu` VALUES (14, 13, 'Altas-Category', '图集分类', 'category', 'atlas/category', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1589783179, 1589783179, NULL);
INSERT INTO `tn_auth_menu` VALUES (15, 13, 'Atlas-List', '图集列表', 'list', 'atlas/list', '', '', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1590039557, 1590039557, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_auth_role
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_role`;
CREATE TABLE `tn_auth_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '权限角色主键id',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '角色标题',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '角色名称',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '角色状态（1 打开 ；0 关闭）',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_title_name` (`title`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='保存用户权限角色信息';

-- ----------------------------
-- Records of tn_auth_role
-- ----------------------------
BEGIN;
INSERT INTO `tn_auth_role` VALUES (1, '超级管理员', 'super_admin', 1, 1586937088, 1590212211, NULL);
INSERT INTO `tn_auth_role` VALUES (2, '管理员', 'admin', 1, 1586937444, 1588852589, NULL);
INSERT INTO `tn_auth_role` VALUES (3, '访客', 'guest', 1, 1588815255, 1588821494, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_auth_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_role_menu`;
CREATE TABLE `tn_auth_role_menu` (
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `menu_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '菜单id',
  UNIQUE KEY `role_menu_id` (`role_id`,`menu_id`),
  KEY `role_id` (`role_id`),
  KEY `menu_id` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='保存角色与菜单之间的数据';

-- ----------------------------
-- Records of tn_auth_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `tn_auth_role_menu` VALUES (1, 1);
INSERT INTO `tn_auth_role_menu` VALUES (1, 2);
INSERT INTO `tn_auth_role_menu` VALUES (1, 3);
INSERT INTO `tn_auth_role_menu` VALUES (1, 4);
INSERT INTO `tn_auth_role_menu` VALUES (1, 5);
INSERT INTO `tn_auth_role_menu` VALUES (1, 6);
INSERT INTO `tn_auth_role_menu` VALUES (1, 7);
INSERT INTO `tn_auth_role_menu` VALUES (1, 8);
INSERT INTO `tn_auth_role_menu` VALUES (1, 9);
INSERT INTO `tn_auth_role_menu` VALUES (1, 10);
INSERT INTO `tn_auth_role_menu` VALUES (1, 11);
INSERT INTO `tn_auth_role_menu` VALUES (1, 12);
INSERT INTO `tn_auth_role_menu` VALUES (1, 13);
INSERT INTO `tn_auth_role_menu` VALUES (1, 14);
INSERT INTO `tn_auth_role_menu` VALUES (1, 15);
INSERT INTO `tn_auth_role_menu` VALUES (2, 1);
INSERT INTO `tn_auth_role_menu` VALUES (2, 2);
INSERT INTO `tn_auth_role_menu` VALUES (2, 3);
INSERT INTO `tn_auth_role_menu` VALUES (2, 4);
INSERT INTO `tn_auth_role_menu` VALUES (2, 5);
INSERT INTO `tn_auth_role_menu` VALUES (2, 6);
INSERT INTO `tn_auth_role_menu` VALUES (2, 7);
INSERT INTO `tn_auth_role_menu` VALUES (2, 8);
INSERT INTO `tn_auth_role_menu` VALUES (3, 1);
INSERT INTO `tn_auth_role_menu` VALUES (3, 2);
INSERT INTO `tn_auth_role_menu` VALUES (3, 3);
INSERT INTO `tn_auth_role_menu` VALUES (3, 4);
INSERT INTO `tn_auth_role_menu` VALUES (3, 5);
INSERT INTO `tn_auth_role_menu` VALUES (3, 6);
INSERT INTO `tn_auth_role_menu` VALUES (3, 7);
COMMIT;

-- ----------------------------
-- Table structure for tn_auth_role_rule
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_role_rule`;
CREATE TABLE `tn_auth_role_rule` (
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '权限id',
  UNIQUE KEY `role_rule_id` (`role_id`,`rule_id`),
  KEY `role_id` (`role_id`),
  KEY `rule_id` (`rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='保存权限角色和权限规则之间的关系';

-- ----------------------------
-- Records of tn_auth_role_rule
-- ----------------------------
BEGIN;
INSERT INTO `tn_auth_role_rule` VALUES (1, 2);
INSERT INTO `tn_auth_role_rule` VALUES (1, 3);
INSERT INTO `tn_auth_role_rule` VALUES (1, 4);
INSERT INTO `tn_auth_role_rule` VALUES (1, 5);
INSERT INTO `tn_auth_role_rule` VALUES (1, 6);
INSERT INTO `tn_auth_role_rule` VALUES (1, 7);
INSERT INTO `tn_auth_role_rule` VALUES (1, 8);
INSERT INTO `tn_auth_role_rule` VALUES (1, 9);
INSERT INTO `tn_auth_role_rule` VALUES (1, 10);
INSERT INTO `tn_auth_role_rule` VALUES (1, 11);
INSERT INTO `tn_auth_role_rule` VALUES (1, 16);
INSERT INTO `tn_auth_role_rule` VALUES (1, 17);
INSERT INTO `tn_auth_role_rule` VALUES (1, 18);
INSERT INTO `tn_auth_role_rule` VALUES (1, 19);
INSERT INTO `tn_auth_role_rule` VALUES (1, 20);
INSERT INTO `tn_auth_role_rule` VALUES (1, 21);
INSERT INTO `tn_auth_role_rule` VALUES (1, 22);
INSERT INTO `tn_auth_role_rule` VALUES (1, 23);
INSERT INTO `tn_auth_role_rule` VALUES (1, 24);
INSERT INTO `tn_auth_role_rule` VALUES (1, 25);
INSERT INTO `tn_auth_role_rule` VALUES (1, 26);
INSERT INTO `tn_auth_role_rule` VALUES (1, 27);
INSERT INTO `tn_auth_role_rule` VALUES (1, 28);
INSERT INTO `tn_auth_role_rule` VALUES (1, 29);
INSERT INTO `tn_auth_role_rule` VALUES (1, 30);
INSERT INTO `tn_auth_role_rule` VALUES (1, 31);
INSERT INTO `tn_auth_role_rule` VALUES (1, 32);
INSERT INTO `tn_auth_role_rule` VALUES (1, 33);
INSERT INTO `tn_auth_role_rule` VALUES (1, 34);
INSERT INTO `tn_auth_role_rule` VALUES (1, 35);
INSERT INTO `tn_auth_role_rule` VALUES (1, 36);
INSERT INTO `tn_auth_role_rule` VALUES (1, 37);
INSERT INTO `tn_auth_role_rule` VALUES (1, 38);
INSERT INTO `tn_auth_role_rule` VALUES (1, 39);
INSERT INTO `tn_auth_role_rule` VALUES (1, 40);
INSERT INTO `tn_auth_role_rule` VALUES (1, 43);
INSERT INTO `tn_auth_role_rule` VALUES (1, 44);
INSERT INTO `tn_auth_role_rule` VALUES (1, 45);
INSERT INTO `tn_auth_role_rule` VALUES (1, 46);
INSERT INTO `tn_auth_role_rule` VALUES (1, 47);
INSERT INTO `tn_auth_role_rule` VALUES (1, 48);
INSERT INTO `tn_auth_role_rule` VALUES (1, 49);
INSERT INTO `tn_auth_role_rule` VALUES (1, 50);
INSERT INTO `tn_auth_role_rule` VALUES (1, 51);
INSERT INTO `tn_auth_role_rule` VALUES (1, 52);
INSERT INTO `tn_auth_role_rule` VALUES (1, 54);
INSERT INTO `tn_auth_role_rule` VALUES (1, 55);
INSERT INTO `tn_auth_role_rule` VALUES (1, 56);
INSERT INTO `tn_auth_role_rule` VALUES (1, 59);
INSERT INTO `tn_auth_role_rule` VALUES (1, 60);
INSERT INTO `tn_auth_role_rule` VALUES (1, 61);
INSERT INTO `tn_auth_role_rule` VALUES (1, 62);
INSERT INTO `tn_auth_role_rule` VALUES (1, 63);
INSERT INTO `tn_auth_role_rule` VALUES (1, 64);
INSERT INTO `tn_auth_role_rule` VALUES (1, 65);
INSERT INTO `tn_auth_role_rule` VALUES (1, 67);
INSERT INTO `tn_auth_role_rule` VALUES (2, 2);
INSERT INTO `tn_auth_role_rule` VALUES (2, 3);
INSERT INTO `tn_auth_role_rule` VALUES (2, 6);
INSERT INTO `tn_auth_role_rule` VALUES (2, 8);
INSERT INTO `tn_auth_role_rule` VALUES (2, 9);
INSERT INTO `tn_auth_role_rule` VALUES (2, 10);
INSERT INTO `tn_auth_role_rule` VALUES (2, 11);
INSERT INTO `tn_auth_role_rule` VALUES (2, 16);
INSERT INTO `tn_auth_role_rule` VALUES (2, 17);
INSERT INTO `tn_auth_role_rule` VALUES (2, 18);
INSERT INTO `tn_auth_role_rule` VALUES (2, 23);
INSERT INTO `tn_auth_role_rule` VALUES (2, 24);
INSERT INTO `tn_auth_role_rule` VALUES (2, 29);
INSERT INTO `tn_auth_role_rule` VALUES (2, 30);
INSERT INTO `tn_auth_role_rule` VALUES (2, 31);
INSERT INTO `tn_auth_role_rule` VALUES (2, 32);
INSERT INTO `tn_auth_role_rule` VALUES (2, 37);
INSERT INTO `tn_auth_role_rule` VALUES (2, 38);
INSERT INTO `tn_auth_role_rule` VALUES (2, 39);
INSERT INTO `tn_auth_role_rule` VALUES (2, 40);
INSERT INTO `tn_auth_role_rule` VALUES (3, 2);
INSERT INTO `tn_auth_role_rule` VALUES (3, 3);
INSERT INTO `tn_auth_role_rule` VALUES (3, 6);
INSERT INTO `tn_auth_role_rule` VALUES (3, 8);
INSERT INTO `tn_auth_role_rule` VALUES (3, 9);
INSERT INTO `tn_auth_role_rule` VALUES (3, 10);
INSERT INTO `tn_auth_role_rule` VALUES (3, 11);
INSERT INTO `tn_auth_role_rule` VALUES (3, 16);
INSERT INTO `tn_auth_role_rule` VALUES (3, 17);
INSERT INTO `tn_auth_role_rule` VALUES (3, 18);
INSERT INTO `tn_auth_role_rule` VALUES (3, 23);
INSERT INTO `tn_auth_role_rule` VALUES (3, 24);
INSERT INTO `tn_auth_role_rule` VALUES (3, 29);
INSERT INTO `tn_auth_role_rule` VALUES (3, 30);
INSERT INTO `tn_auth_role_rule` VALUES (3, 31);
INSERT INTO `tn_auth_role_rule` VALUES (3, 32);
INSERT INTO `tn_auth_role_rule` VALUES (3, 37);
INSERT INTO `tn_auth_role_rule` VALUES (3, 38);
COMMIT;

-- ----------------------------
-- Table structure for tn_auth_role_user
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_role_user`;
CREATE TABLE `tn_auth_role_user` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员id',
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '权限角色id',
  UNIQUE KEY `uid_role_id` (`user_id`,`role_id`),
  KEY `uid` (`user_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户与权限组之间的关系';

-- ----------------------------
-- Records of tn_auth_role_user
-- ----------------------------
BEGIN;
INSERT INTO `tn_auth_role_user` VALUES (1, 1);
INSERT INTO `tn_auth_role_user` VALUES (2, 2);
INSERT INTO `tn_auth_role_user` VALUES (3, 1);
INSERT INTO `tn_auth_role_user` VALUES (4, 2);
COMMIT;

-- ----------------------------
-- Table structure for tn_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_rule`;
CREATE TABLE `tn_auth_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '权限规则主键id',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '权限规则父级id',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '权限规则标题',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '权限规则标识',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '规则验证方式，如果为1则可以验证condition中的规则',
  `sort` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '权限规则排序',
  `public_rule` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为公用权限（1 是 0 否）',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '权限状态（1 开启 0 关闭）',
  `condition` varchar(255) NOT NULL DEFAULT '' COMMENT '多重验证用到',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tn_auth_rule
-- ----------------------------
BEGIN;
INSERT INTO `tn_auth_rule` VALUES (1, 0, '管理员', 'admin', 1, 1, 0, 1, '', 1588659366, 1588668625, NULL);
INSERT INTO `tn_auth_rule` VALUES (2, 1, '获取管理员列表数据', 'admin/getlist', 1, 1, 0, 1, '', 1588659441, 1588668625, NULL);
INSERT INTO `tn_auth_rule` VALUES (3, 1, '根据id获取管理员信息', 'admin/getbyid', 1, 2, 0, 1, '', 1588659466, 1588668625, NULL);
INSERT INTO `tn_auth_rule` VALUES (4, 1, '添加管理员', 'admin/addadmin', 1, 3, 0, 1, '', 1588667563, 1588668620, NULL);
INSERT INTO `tn_auth_rule` VALUES (5, 1, '编辑管理员', 'admin/editadmin', 1, 4, 0, 1, '', 1588667651, 1588668625, NULL);
INSERT INTO `tn_auth_rule` VALUES (6, 1, '更新管理员信息', 'admin/upadteadmin', 1, 5, 0, 1, '', 1588667739, 1588668625, NULL);
INSERT INTO `tn_auth_rule` VALUES (7, 1, '删除指定的管理员信息', 'admin/deleteadmin', 1, 6, 0, 1, '', 1588667833, 1588668625, NULL);
INSERT INTO `tn_auth_rule` VALUES (8, 1, '管理员登陆', 'admin/login', 1, 7, 1, 1, '', 1588668922, 1588734790, NULL);
INSERT INTO `tn_auth_rule` VALUES (9, 1, '管理员退出', 'admin/logout', 1, 8, 1, 1, '', 1588668944, 1588668998, NULL);
INSERT INTO `tn_auth_rule` VALUES (10, 1, '获取对应管理员的路由信息', 'admin/routes', 1, 9, 1, 1, '', 1588669017, 1588669024, NULL);
INSERT INTO `tn_auth_rule` VALUES (11, 1, '获取对应管理员的信息', 'admin/info', 1, 10, 1, 1, '', 1588669163, 1588669166, NULL);
INSERT INTO `tn_auth_rule` VALUES (12, 0, '权限管理', 'auth', 1, 2, 0, 1, '', 1588669462, 1588669462, NULL);
INSERT INTO `tn_auth_rule` VALUES (13, 12, '权限角色', 'authrole', 1, 1, 0, 1, '', 1588669501, 1588669501, NULL);
INSERT INTO `tn_auth_rule` VALUES (14, 12, '权限菜单', 'authmenu', 1, 2, 0, 1, '', 1588669549, 1588669549, NULL);
INSERT INTO `tn_auth_rule` VALUES (15, 12, '权限规则', 'authrule', 1, 3, 0, 1, '', 1588669563, 1588669563, NULL);
INSERT INTO `tn_auth_rule` VALUES (16, 13, '获取权限角色列表数据', 'authrole/getlist', 1, 1, 0, 1, '', 1588669593, 1588669593, NULL);
INSERT INTO `tn_auth_rule` VALUES (17, 13, '获取权限角色的名称数据', 'authrole/getall', 1, 2, 0, 1, '', 1588669618, 1588827803, NULL);
INSERT INTO `tn_auth_rule` VALUES (18, 13, '根据id获取指定的数据', 'authrole/getbyid', 1, 3, 0, 1, '', 1588669704, 1588669704, NULL);
INSERT INTO `tn_auth_rule` VALUES (19, 13, '添加权限角色', 'authrole/addrole', 1, 4, 0, 1, '', 1588669723, 1588669723, NULL);
INSERT INTO `tn_auth_rule` VALUES (20, 13, '编辑权限角色', 'authrole/editrole', 1, 5, 0, 1, '', 1588669941, 1588669941, NULL);
INSERT INTO `tn_auth_rule` VALUES (21, 13, '更新权限角色信息', 'authrole/updaterole', 1, 6, 0, 1, '', 1588677433, 1588677433, NULL);
INSERT INTO `tn_auth_rule` VALUES (22, 13, '删除指定的权限信息', 'authrole/deleterole', 1, 7, 0, 1, '', 1588677457, 1588677457, NULL);
INSERT INTO `tn_auth_rule` VALUES (23, 14, '获取表格树形数据', 'authmenu/gettabletree', 1, 1, 0, 1, '', 1588677512, 1588677512, NULL);
INSERT INTO `tn_auth_rule` VALUES (24, 14, '根据id获取角色权限菜单的信息', 'authmenu/getbyid', 1, 2, 0, 1, '', 1588677542, 1588677542, NULL);
INSERT INTO `tn_auth_rule` VALUES (25, 14, '添加角色权限菜单', 'authmenu/addmenu', 1, 3, 0, 1, '', 1588677561, 1588677561, NULL);
INSERT INTO `tn_auth_rule` VALUES (26, 14, '编辑角色权限菜单', 'authmenu/editmenu', 1, 4, 0, 1, '', 1588677578, 1588677578, NULL);
INSERT INTO `tn_auth_rule` VALUES (27, 14, '更新角色权限菜单信息', 'authmenu/updatemenu', 1, 5, 0, 1, '', 1588677820, 1588677820, NULL);
INSERT INTO `tn_auth_rule` VALUES (28, 14, '删除角色权限菜单信息', 'authmenu/deletemenu', 1, 6, 0, 1, '', 1588677839, 1588677839, NULL);
INSERT INTO `tn_auth_rule` VALUES (29, 14, '获取角色权限菜单的所有节点信息', 'authmenu/getallmenunode', 1, 7, 0, 1, '', 1588677859, 1588677859, NULL);
INSERT INTO `tn_auth_rule` VALUES (30, 14, '获取指定父节点下的子节点数量', 'authmenu/getchildrencount', 1, 8, 0, 1, '', 1588677882, 1588677882, NULL);
INSERT INTO `tn_auth_rule` VALUES (31, 15, '获取表格树形数据', 'authrule/gettabletree', 1, 1, 0, 1, '', 1588678146, 1588678146, NULL);
INSERT INTO `tn_auth_rule` VALUES (32, 15, '根据id获取角色权限规则的信息', 'authrule/getbyid', 1, 2, 0, 1, '', 1588678483, 1588678483, NULL);
INSERT INTO `tn_auth_rule` VALUES (33, 15, '添加角色权限规则', 'authrule/addrule', 1, 3, 0, 1, '', 1588678597, 1588678597, NULL);
INSERT INTO `tn_auth_rule` VALUES (34, 15, '编辑角色权限规则', 'authrule/editrule', 1, 4, 0, 1, '', 1588678709, 1588678709, NULL);
INSERT INTO `tn_auth_rule` VALUES (35, 15, '更新角色权限规则信息', 'authrule/updaterule', 1, 5, 0, 1, '', 1588678730, 1588678730, NULL);
INSERT INTO `tn_auth_rule` VALUES (36, 15, '删除角色权限规则信息', 'authrule/deleterule', 1, 6, 0, 1, '', 1588678884, 1588678884, NULL);
INSERT INTO `tn_auth_rule` VALUES (37, 15, '获取角色权限规则的所有节点信息', 'authrule/getallrulenode', 1, 7, 0, 1, '', 1588678903, 1588678903, NULL);
INSERT INTO `tn_auth_rule` VALUES (38, 15, '获取指定父节点下的子节点数量', 'authrule/getchildrencount', 1, 8, 0, 1, '', 1588678923, 1588734805, NULL);
INSERT INTO `tn_auth_rule` VALUES (39, 14, '获取树形结构数据', 'authmenu/getelementtree', 1, 9, 0, 1, '', 1588852520, 1588852520, NULL);
INSERT INTO `tn_auth_rule` VALUES (40, 15, '获取树形结构数据', 'authrule/getelementtree', 1, 9, 0, 1, '', 1588852541, 1588852541, NULL);
INSERT INTO `tn_auth_rule` VALUES (41, 0, '系统', 'system', 1, 3, 0, 1, '', 1589423745, 1589423783, NULL);
INSERT INTO `tn_auth_rule` VALUES (42, 41, '系统设置', 'systemconfig', 1, 1, 0, 1, '', 1589423773, 1589423792, NULL);
INSERT INTO `tn_auth_rule` VALUES (43, 42, '获取表格树形数据', 'systemconfig/gettabletree', 1, 1, 0, 1, '', 1589423880, 1589423880, NULL);
INSERT INTO `tn_auth_rule` VALUES (44, 42, '根据id获取配置对应的信息', 'systemconfig/getbyid', 1, 2, 0, 1, '', 1589423899, 1589423899, NULL);
INSERT INTO `tn_auth_rule` VALUES (45, 42, '添加系统配置项信息', 'systemconfig/addconfig', 1, 3, 0, 1, '', 1589423922, 1589423922, NULL);
INSERT INTO `tn_auth_rule` VALUES (46, 42, '编辑系统配置项信息', 'systemconfig/editconfig', 1, 4, 0, 1, '', 1589423948, 1589423948, NULL);
INSERT INTO `tn_auth_rule` VALUES (47, 42, '更新系统配置项信息', 'systemconfig/updateconfig', 1, 5, 0, 1, '', 1589423974, 1589423974, NULL);
INSERT INTO `tn_auth_rule` VALUES (48, 42, '删除系统配置项信息', 'systemconfig/deleteconfig', 1, 6, 0, 1, '', 1589424000, 1589424000, NULL);
INSERT INTO `tn_auth_rule` VALUES (49, 42, '更新系统配置信息', 'systemconfig/commitconfigdata', 1, 7, 0, 1, '', 1589424023, 1589424023, NULL);
INSERT INTO `tn_auth_rule` VALUES (50, 42, '获取配置菜单信息', 'systemconfig/getmenudata', 1, 8, 0, 1, '', 1589424044, 1589424044, NULL);
INSERT INTO `tn_auth_rule` VALUES (51, 42, '获取系统配置的所有父节点信息', 'systemconfig/getallconfigparentnode', 1, 9, 0, 1, '', 1589424075, 1589424075, NULL);
INSERT INTO `tn_auth_rule` VALUES (52, 42, '获取指定父节点下的子节点数量', 'systemconfig/getchildrencount', 1, 10, 0, 1, '', 1589424117, 1589424117, NULL);
INSERT INTO `tn_auth_rule` VALUES (53, 41, '系统日志', 'systemlog', 1, 2, 0, 1, '', 1590208320, 1590208347, NULL);
INSERT INTO `tn_auth_rule` VALUES (54, 53, '获取日志列表信息', 'systemlog/getlist', 1, 1, 0, 1, '', 1590208691, 1590208691, NULL);
INSERT INTO `tn_auth_rule` VALUES (55, 53, '删除指定的日志信息', 'systemlog/deletelog', 1, 2, 0, 1, '', 1590208715, 1590208715, NULL);
INSERT INTO `tn_auth_rule` VALUES (56, 53, '清空日志信息', 'systemlog/clearalllog', 1, 3, 0, 1, '', 1590208737, 1590208737, NULL);
INSERT INTO `tn_auth_rule` VALUES (57, 0, '图集', 'Atlas', 1, 4, 0, 1, '', 1590208857, 1590212008, NULL);
INSERT INTO `tn_auth_rule` VALUES (58, 57, '图集栏目管理', 'atlascategory', 1, 1, 0, 1, '', 1590209372, 1590209372, NULL);
INSERT INTO `tn_auth_rule` VALUES (59, 58, '获取图集类目分类的分页数据', 'atlascategory/getlist', 1, 1, 0, 1, '', 1590211662, 1590211662, NULL);
INSERT INTO `tn_auth_rule` VALUES (60, 58, '根据id获取图集类目分类的数据', 'atlascategory/getbyid', 1, 2, 0, 1, '', 1590211816, 1590211816, NULL);
INSERT INTO `tn_auth_rule` VALUES (61, 58, '获取图集类目分类的全部名称信息', 'atlascategory/getallname', 1, 3, 1, 1, '', 1590211855, 1590211855, NULL);
INSERT INTO `tn_auth_rule` VALUES (62, 58, '添加图集类目分类信息', 'atlascategory/addatlascategory', 1, 4, 0, 1, '', 1590211888, 1590211888, NULL);
INSERT INTO `tn_auth_rule` VALUES (63, 58, '编辑图集类目分类信息', 'atlascategory/editatlascategory', 1, 5, 0, 1, '', 1590211919, 1590211919, NULL);
INSERT INTO `tn_auth_rule` VALUES (64, 58, '更新图集类目分类信息', 'atlascategory/updateatlascategory', 1, 6, 0, 1, '', 1590211956, 1590211956, NULL);
INSERT INTO `tn_auth_rule` VALUES (65, 58, '删除图集类目分类信息', 'atlascategory/deleteatalscategory', 1, 7, 0, 1, '', 1590211978, 1590211978, NULL);
INSERT INTO `tn_auth_rule` VALUES (66, 57, '图集管理', 'atlasmananger', 1, 2, 0, 1, '', 1590212040, 1590212040, NULL);
INSERT INTO `tn_auth_rule` VALUES (67, 66, '修改图集中图片的所属栏目', 'atlas/changecategory', 1, 1, 1, 1, '', 1590212181, 1590212181, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_system_config
-- ----------------------------
DROP TABLE IF EXISTS `tn_system_config`;
CREATE TABLE `tn_system_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '设置的主键id',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '设置的父级id',
  `cn_name` varchar(80) NOT NULL DEFAULT '' COMMENT '配置对应的中文名',
  `en_name` varchar(80) NOT NULL DEFAULT '' COMMENT '配置对应的字段名称',
  `values` text NOT NULL COMMENT '配置的可选值',
  `tips` varchar(255) NOT NULL COMMENT '配置提示值',
  `value` text NOT NULL COMMENT '配置的值',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '配置的类型 1：文本输入框 2：单选按钮 3：复选框 4：下拉菜单 5：文本域 6：富文本 7：单图片 8：多图片 9：附件上传',
  `sort` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '配置的排序序号',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '配置项的状态（1 开启 0 关闭）',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_en_name` (`id`,`en_name`),
  KEY `en_name` (`en_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COMMENT='保存系统配置的相关信息';

-- ----------------------------
-- Records of tn_system_config
-- ----------------------------
BEGIN;
INSERT INTO `tn_system_config` VALUES (1, 0, '网站设置', '', '', '', '', 0, 1, 1, 1588944799, 1588944799, NULL);
INSERT INTO `tn_system_config` VALUES (2, 1, '标题', 'site_title', '', '', '图鸟科技后台管理系统', 1, 1, 1, 1589362296, 1590208066, NULL);
INSERT INTO `tn_system_config` VALUES (3, 1, '域名', 'site_url', '', '请以http://或者https://开头', 'http://vue.bof.demo.tuniaokj.com', 1, 2, 1, 1589362366, 1590208066, NULL);
INSERT INTO `tn_system_config` VALUES (4, 1, '作者', 'site_author', '', '', '图鸟科技', 1, 3, 1, 1589367023, 1590208066, NULL);
INSERT INTO `tn_system_config` VALUES (5, 1, '备案号', 'site_record', '', '', '', 1, 4, 1, 1589367043, 1590208066, NULL);
INSERT INTO `tn_system_config` VALUES (6, 1, 'token缓存时间', 'site_token_expire_in', '', '设置token的缓存时间，以秒作为单位。提示：通常根据实际情况进行设置', '7200', 10, 5, 1, 1589367104, 1590208066, NULL);
INSERT INTO `tn_system_config` VALUES (7, 1, '允许上传的最大文件大小', 'site_file_max_size', '', '设置上传文件的最大大小，以KB为单位。提示：1 M = 1024 KB', '2048', 10, 6, 1, 1589367156, 1590208066, NULL);
INSERT INTO `tn_system_config` VALUES (8, 1, '开启https', 'site_https_on', '开启,关闭', '提示：此选项是开启或者关闭与微信服务器通讯的时候是否开启https(在Linus下部署请选中该选项)', '关闭', 2, 7, 1, 1589367240, 1590208066, NULL);
INSERT INTO `tn_system_config` VALUES (9, 1, 'Logo', 'site_logo', '', '', '/storage/uploads/img_list/c6/aab26376f3f6dc3c5a737784e45985.png', 7, 8, 1, 1589367265, 1590208066, NULL);
INSERT INTO `tn_system_config` VALUES (10, 0, '后台设置', '', '', '', '', 0, 2, 1, 1589367318, 1589367318, NULL);
INSERT INTO `tn_system_config` VALUES (11, 10, '权限验证', 'bk_auth_on', '开启,关闭', '', '关闭', 2, 1, 1, 1589367396, 1590208066, NULL);
INSERT INTO `tn_system_config` VALUES (12, 10, '日志记录', 'bk_log_on', '开启,关闭', '', '关闭', 2, 2, 1, 1589367443, 1590208066, NULL);
INSERT INTO `tn_system_config` VALUES (13, 10, '多图片', 'multi_pic', '', '', '[]', 8, 3, 1, 1589720725, 1590208066, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_system_log
-- ----------------------------
DROP TABLE IF EXISTS `tn_system_log`;
CREATE TABLE `tn_system_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '系统日志的主键id',
  `user_name` varchar(20) NOT NULL DEFAULT '' COMMENT '操作的用户名',
  `controller` varchar(150) NOT NULL DEFAULT '' COMMENT '操作的控制器',
  `method` varchar(150) NOT NULL DEFAULT '' COMMENT '操作的方法',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '日志信息',
  `ip_address` varchar(60) NOT NULL DEFAULT '' COMMENT '操作的ip地址',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='保存系统操作日志';

SET FOREIGN_KEY_CHECKS = 1;
