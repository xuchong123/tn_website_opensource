import request from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function getAtlasListData(params) {
  return request({
    url: 'atlas/list',
    method: 'get',
    params
  })
}

export function deleteAtlasImage(data) {
  return request({
    url: 'atlas/delete',
    method: 'delete',
    data: { ...data }
  })
}

export function changeCategory(data) {
  return request({
    url: api_prefix + 'atlas/change_category',
    method: 'put',
    data
  })
}
