import reuqest from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function getTableTreeData(params) {
  return reuqest({
    url: api_prefix + 'auth_menu/table_tree',
    method: 'get',
    params
  })
}

export function getMenuElementTreeData() {
  return reuqest({
    url: api_prefix + 'auth_menu/element_tree',
    method: 'get'
  })
}

export function getAuthMenuByID(id) {
  return reuqest({
    url: api_prefix + 'auth_menu/get_id',
    method: 'get',
    params: { id }
  })
}

export function getAllMenuNode() {
  return reuqest({
    url: api_prefix + 'auth_menu/all_menu',
    method: 'get'
  })
}

export function getChildrenCount(pid) {
  return reuqest({
    url: api_prefix + 'auth_menu/get_children_count',
    method: 'get',
    params: { pid }
  })
}

export function addAuthMenu(data) {
  return reuqest({
    url: api_prefix + 'auth_menu/add',
    method: 'post',
    data
  })
}

export function editAuthMenu(data) {
  return reuqest({
    url: api_prefix + 'auth_menu/edit',
    method: 'put',
    data
  })
}

export function updateAuthMenu(data) {
  return reuqest({
    url: api_prefix + 'auth_menu/update',
    method: 'put',
    data
  })
}

export function deleteAuthMenu(ids) {
  return reuqest({
    url: api_prefix + 'auth_menu/delete',
    method: 'delete',
    data: { ids }
  })
}
