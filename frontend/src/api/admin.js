import request from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function login(data) {
  return request({
    url: api_prefix + 'admin/login',
    method: 'post',
    data
  })
}

export function getRoutes() {
  return request({
    url: api_prefix + 'admin/routes',
    method: 'get'
  })
}

export function getInfo() {
  return request({
    url: api_prefix + 'admin/info',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: api_prefix + 'admin/logout',
    method: 'post'
  })
}

export function listAdmin(params) {
  return request({
    url: api_prefix + 'admin/list',
    method: 'get',
    params
  })
}

export function getAdminByID(id) {
  return request({
    url: api_prefix + 'admin/get_id',
    method: 'get',
    params: { id }
  })
}

export function addAdmin(data) {
  return request({
    url: api_prefix + '/admin/add',
    method: 'post',
    data
  })
}

export function editAdmin(data) {
  return request({
    url: api_prefix + 'admin/edit',
    method: 'put',
    data
  })
}

export function updateAdmin(data) {
  return request({
    url: api_prefix + 'admin/update',
    method: 'put',
    data
  })
}

export function getTokenByCode(data) {
  return request({
    url: api_prefix + 'admin/get_we_chat_code_token',
    method: 'post',
    data
  })
}

export function getWeChatUserList(params) {
  return request({
    url: api_prefix + 'admin/list_we_chat_admin',
    method: 'get',
    params
  })
}

export function deleteAdmin(ids) {
  return request({
    url: api_prefix + 'admin/delete',
    method: 'delete',
    data: { ids }
  })
}

export function deleteWeChatAdmin(data) {
  return request({
    url: api_prefix + 'admin/delete_we_chat_admin',
    method: 'delete',
    data
  })
}
