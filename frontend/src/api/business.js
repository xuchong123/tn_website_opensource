import request from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function getBusinessList(params) {
  return request({
    url: api_prefix + 'business/list',
    method: 'get',
    params
  })
}

export function getBusinessByID(id) {
  return request({
    url: api_prefix + 'business/get_id',
    method: 'get',
    params: { id }
  })
}

export function getBusinessOperationUser(params) {
  return request({
    url: api_prefix + 'business/get_operation_user',
    method: 'get',
    params
  })
}

export function addBusiness(data) {
  return request({
    url: api_prefix + 'business/add',
    method: 'post',
    data
  })
}

export function editBusiness(data) {
  return request({
    url: api_prefix + 'business/edit',
    method: 'put',
    data
  })
}

export function updateBusiness(data) {
  return request({
    url: api_prefix + 'business/update',
    method: 'put',
    data
  })
}

export function deleteBusiness(ids) {
  return request({
    url: api_prefix + 'business/delete',
    method: 'delete',
    data: { ids }
  })
}
